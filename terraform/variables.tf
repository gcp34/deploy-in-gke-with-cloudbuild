variable "project_id" {
  type        = string
  description = "project id required"
}
variable "project_name" {
 type        = string
 description = "project name in which demo deploy"
}
variable "org_id" {
 description = "Organization ID in which project created"
}
variable "network_name" {
 type        = string
 description = "network name for the project"
}
variable "billing_account" {
 type        = string
 description = "Billing Account for the project"
}
variable "region" {
 type        = string
 description = "Region where deploy"
}