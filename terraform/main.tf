####################################################################################
# Create a new project named "GKE Demo Project" with ID : "gke-project-customer"   #
####################################################################################
module "project" {
  source                  = "terraform-google-modules/project-factory/google"
  project_id              = var.project_id
  name                    = var.project_name
  org_id                  = var.org_id
  billing_account         = var.billing_account

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
    "sourcerepo.googleapis.com",
    "clouddeploy.googleapis.com",
    "artifactregistry.googleapis.com",
    "cloudresourcemanager.googleapis.com",
  ]
}

####################################################################################
# Resource for Network Creation                                                    #
####################################################################################

resource "google_compute_network" "default" {
  project                 = module.project.project_id
  name                    = var.network_name
  description             = "Default network"
  routing_mode            = "REGIONAL" 
  auto_create_subnetworks = true
  mtu                     = 1460
  depends_on              = [module.project]
}

resource "google_compute_firewall" "default" {
  name                    = "default-allow-icmp"
  project                 = module.project.project_id
  network                 = google_compute_network.default.name
  description             = "Allows ICMP connections from any source to any instance on the network"
  direction               = "INGRESS"
  priority                = 65534
  depends_on              = [google_compute_network.default]

  
  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

####################################################################################
# Resource for GKE Cluster                                                         #
####################################################################################
# Service Account creation
module "service_accounts" {
    source        = "terraform-google-modules/service-accounts/google"
    version       = "~> 3.0"
    project_id    = module.project.project_id
    prefix        = "gke-sa"
    names         = ["gke-sa"]
    project_roles = [
      "${var.project_id}=>roles/compute.viewer",
      "${var.project_id}=>roles/compute.securityAdmin",
      "${var.project_id}=>roles/container.clusterAdmin",
      "${var.project_id}=>roles/container.developer",
      "${var.project_id}=>roles/iam.serviceAccountAdmin",
      "${var.project_id}=>roles/iam.serviceAccountUser",
      "${var.project_id}=>roles/resourcemanager.projectIamAdmin",
    ]
    depends_on    = [module.project]
}

# Cluster and Node pool creation
module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  project_id                 = module.project.project_id
  name                       = "mycluster-dev-cluster"
  region                     = var.region
  network                    = var.network_name
  subnetwork                 = var.region
  ip_range_pods              = "${var.region}-01-pods"
  ip_range_services          = "${var.region}-01-services"
  http_load_balancing        = true
  network_policy             = false
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = false
  depends_on              = [module.project, module.service_accounts, google_compute_network.default]

  node_pools = [
    {
      name                      = "mycluster-dev-node-pool"
      machine_type              = "n2-standard-4"
      node_locations            = var.region
      min_count                 = 1
      max_count                 = 3
      local_ssd_count           = 0
      spot                      = false
      disk_size_gb              = 100
      disk_type                 = "pd-standard"
      image_type                = "COS_CONTAINERD"
      enable_gcfs               = false
      enable_gvnic              = false
      auto_repair               = true
      auto_upgrade              = true
      service_account           = module.service_accounts.service_account.email
      preemptible               = false
      initial_node_count        = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {
        environment = "dev"
    }

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    all = {}

    default-node-pool = {
      node-pool-metadata-custom-value = "my-node-pool",
      disable-legacy-endpoints=true 
    }
  }

  node_pools_taints = {
    all = []
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}