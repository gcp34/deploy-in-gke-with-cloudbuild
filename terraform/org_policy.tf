module "org-policy-requireOsLogin" {
  source = "terraform-google-modules/org-policy/google"

  policy_for  = "project"
  constraint  = "compute.requireOsLogin"
  project_id  = var.project_id
  policy_type = "boolean"
  enforce     = false
}

module "org-policy-requireShieldedVm" {
  source = "terraform-google-modules/org-policy/google"

  policy_for  = "project"
  constraint  = "compute.requireShieldedVm"
  project_id  = var.project_id
  policy_type = "boolean"
  enforce     = false
}

module "org-policy-vmCanIpForward" {
  source = "terraform-google-modules/org-policy/google"

  policy_for  = "project"
  constraint  = "compute.vmCanIpForward"
  project_id  = var.project_id
  policy_type = "list"
  enforce     = false
}

module "org-policy-vmExternalIpAccess" {
  source = "terraform-google-modules/org-policy/google"

  policy_for  = "project"
  constraint  = "compute.vmExternalIpAccess"
  project_id  = var.project_id
  policy_type = "list"
  enforce     = false
}

module "org-policy-restrictVpcPeering" {
  source = "terraform-google-modules/org-policy/google"

  policy_for  = "project"
  constraint  = "compute.restrictVpcPeering"
  project_id  = var.project_id
  policy_type = "list"
  enforce     = false
}