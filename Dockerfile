FROM python:3

ADD python-webapp.py python-webapp.py

EXPOSE 80

CMD ["python3","python-webapp.py"]